"""Tweak our initial users -- they would love to more points and status and a place to live!

- Increase the point_balance for user 1 to 5000
- Add a location for user 2, assuming they live in the USA
- Change the tier for user 3 to Silver

Revision ID: 29ea283f0b44
Revises: 00000000
Create Date: 2019-06-15 15:03:01.247444

"""

# revision identifiers, used by Alembic.
revision = '29ea283f0b44'
down_revision = '00000000'

from alembic import op


def upgrade():
    op.execute('''
        UPDATE user SET point_balance = 5000 WHERE user_id = 1
    ''')

    op.execute('''
        INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES
            (2, 'LOCATION', 'USA')
    ''')

    op.execute('''
        UPDATE user SET tier = 'Silver' WHERE user_id = 3
    ''')


def downgrade():
    op.execute('''UPDATE user SET point_balance = 0 WHERE user_id = 1''')
    op.execute('''
        DELETE FROM rel_user
        WHERE
            user_id = 2
            AND rel_lookup = 'LOCATION'
            AND attribute = 'USA'
    ''')
    op.execute('''
        UPDATE user SET tier = 'Carbon' WHERE user_id = 3
    ''')
