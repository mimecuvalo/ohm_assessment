from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
from models import db


@app.route('/community', methods=['GET'])
def community():
    login_user(User.query.get(1))

    recent_users_cursor = db.executeRaw('''
        SELECT user.user_id, display_name, tier, point_balance,
                GROUP_CONCAT(rel_user_multi.attribute SEPARATOR '\n') as phone_number,
                GROUP_CONCAT(DISTINCT rel_user.attribute) as location
            FROM user
            LEFT JOIN rel_user_multi ON user.user_id = rel_user_multi.user_id
            LEFT JOIN rel_user ON user.user_id = rel_user.user_id
            WHERE (rel_user_multi.rel_lookup = 'PHONE' or
                rel_user_multi.user_id IS NULL) and
                (rel_user.rel_lookup = 'LOCATION' or
                    rel_user.user_id IS NULL)
            GROUP BY user.user_id
            ORDER BY signup_date DESC
            LIMIT 5
    ''')
    recent_users = recent_users_cursor.fetchall()

    args = {
        'most_recent_users': recent_users
    }

    return render_template("community.html", **args)