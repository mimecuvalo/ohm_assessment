export PYTHONPATH=$(pwd)
pip install -r requirements.txt

read -s -p "Enter MySQL DB Password: " DB_PASSWORD

setup_db() {
  echo
  echo "Installing $1 DB"

  OHM_DB="ohm_assessment_$1"

  cp config/my_development.cnf.sample "config/my_$1.cnf"
  sed -i '' -e s/password\ =\ root/password\ =\ $DB_PASSWORD/ "config/my_$1.cnf"
  sed -i '' -e s/database\ =\ ohm_assessment/database\ =\ $OHM_DB/ "config/my_$1.cnf"

  mysqladmin -u root -p"$DB_PASSWORD" create $OHM_DB
  mysql -u root -p"$DB_PASSWORD" $OHM_DB < config/seed.sql

  export FLASK_ENVIRONMENT=$1
  alembic upgrade head
}

setup_db development
setup_db test